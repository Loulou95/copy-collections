<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    $batch_no = md5(rand());
    $api = app('ShopifyAPI');

    // Get all collections
    $collection_page = 0;
    $limit_per_page = 250;
    $all_collections = [];

    do {
        $collection_page++;
        $collections = $api->call('get', '/admin/smart_collections.json', [
            'limit' => $limit_per_page,
            'page' => $collection_page
        ])->smart_collections;

        foreach ($collections as $collection) {
            // Unset not needed fields
            unset($collection->id);
            unset($collection->updated_at);
            unset($collection->published_at);
            if (isset($collection->image)) {
                unset($collection->image->created_at);
                unset($collection->image->width);
                unset($collection->image->height);
            }

            $all_collections[] = json_decode(json_encode($collection), true);
        }
    } while (count($collections) == $limit_per_page);

    // Iterate through each collection
    $all_collections = collect($all_collections);
    foreach ($all_collections as $collection) {
        $suffix = ' - Trade';
        $handle_suffix = '-trade';
        $suffix_length = strlen($suffix);

        // Contains the suffix already
        if (substr(strtolower($collection['title']), -$suffix_length) === strtolower($suffix)) {
            \Log::notice('[BATCH ' . $batch_no . '] Ignoring ' . $collection['title'] . ' - ends with suffix');
            continue;
        }

        // Check correspondant
        $correspondant = $all_collections->filter(function ($item) use ($collection, $suffix) {
            return $collection['title'] . $suffix == $item['title'];
        });

        // Has a correspondant
        if ($correspondant->count() > 0) {
            \Log::notice('[BATCH ' . $batch_no . '] Ignoring ' . $collection['title'] . ' - has correspondant');
            continue;
        }

        // Go ahead and post the new one
        $new_collection = $collection;
        $new_collection['title'] = $new_collection['title'] . $suffix;
        $new_collection['handle'] = $new_collection['handle'] . $handle_suffix;

        try {
            $api->call('post', '/admin/smart_collections.json', [
                'smart_collection' => $new_collection
            ]);

            \Log::debug('[BATCH ' . $batch_no . '] Saved new collection ' . $new_collection['title']);
        } catch (\Exception $e) {
            \Log::error('[BATCH ' . $batch_no . '] Can\'t save copy of collection ' . $collection['title'], $new_collection);
        }
    }

    return [ 'status' => 'done' ];
});

